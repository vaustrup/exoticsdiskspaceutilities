import argparse
import logging
import sys 

log = logging.getLogger("deleter")

from rucio.client import Client
from rucio.common.exception import AccessDenied

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--filename",
                    required=True,
                    help="Name of file containing list of datasets to delete. Datasets are required to be listed as scope:datasetname with one dataset per row.")
parser.add_argument("-r", "--rse",
                    default="CERN-PROD_PHYS-EXOTICS",
                    help="Name of RSE to delete rules from.")
parser.add_argument("-d", "--debug",
                    dest="debug", 
                    action="store_true",
                    help="Enable DEBUG information instead of standard INFO level.") 
parser.add_argument("--noconfirm",
                    dest="noconfirm",
                    action="store_true",
                    help="Do not ask for confirmation before deleting rule.")
args = parser.parse_args()

handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter("%(asctime)s [%(levelname)4s]: %(message)s", "%d.%m.%Y %H:%M:%S")
handler.setFormatter(formatter)
log.addHandler(handler)
log.setLevel(logging.INFO)
if args.debug:
    log.setLevel(logging.DEBUG)


client = Client()

containers_to_delete = []
log.info(f"Opening list of datasets to delete from file {args.filename}.")
with open(args.filename, "r") as f:
    for dataset in f:
        # skip empty lines
        if not dataset.strip():
            continue
        scope, name = dataset.split(":")
        name = name.replace('\n', '')
        log.debug(f"Checking parents of dataset {name} in scope {scope}.")
        parents = client.list_parent_dids(scope=scope, name=name)
        for parent in parents:
            if parent not in containers_to_delete:
                log.info(f"Adding {parent['name']} to list of containers to be deleted.")
                containers_to_delete.append(parent)

log.info(f"Looping over containers to find rules to delete on RSE {args.rse}.")
for container in containers_to_delete:
    filters = {"scope": container["scope"], "name": container["name"], "rse_expression": args.rse}
    rules = client.list_replication_rules(filters)
    for rule in rules:
        log.info(f"Deleting rule {rule['id']} for container {container['name']}.")
        if not args.noconfirm:
            confirmation = input("Confirm ([y]es/[n]o): ")
            if not confirmation.lower() in ["y", "yes"]:
                break
        try:
            client.delete_replication_rule(rule_id=rule["id"])
        except AccessDenied:
            current_user = client.whoami()
            log.error(f"Cannot delete rule {rule['id']} due to incorrect access rights. Rule owner is {rule['account']}, you are using account {current_user['account']}.")
            pass
