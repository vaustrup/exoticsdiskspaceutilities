# ExoticsDiskspaceUtilities

A collection of useful scripts for managing the Exotics group disk space.

## archiver.py

Uploading files from local storage to the grid with Rucio can be quite tedious and error prone, especially when uploading large files or a large number of files.
To facilitate this process, the archiver.py script automatically splits large files into smaller chunks of 10GB by default.
Similarly, small files are combined into tarballs of up to 10GB.
The tarballs are then uploaded to a specified grid site and stored in a dataset with a given name.
A list of which files are contained in which tarball is added for later reconstruction of the original files using the reconstruction.py script.
It is possible to speed up the uploading process by running in parallel mode.
For details on the usage check the help text below.

```
usage: archiver.py [-h] [-d] --dataset_name DATASET_NAME [--tmp_dir TMP_DIR] --input_path INPUT_PATH [--size_threshold SIZE_THRESHOLD] --rse RSE [--continue_uploads] [--max_duration MAX_DURATION] [--scope SCOPE] [--lifetime LIFETIME]
                   [--max_workers MAX_WORKERS] [--debug]

optional arguments:
  -h, --help            show this help message and exit
  -d, --dry_run         Collect input files and create tarballs, but do not actually upload them with Rucio.
  --dataset_name DATASET_NAME
                        Name of dataset to register files in.
  --tmp_dir TMP_DIR     Directory to temporarily store file chunks and tarballs to upload. Default: /tmp/$USER.
  --input_path INPUT_PATH
                        Path to the directory containing all files to be uploaded.
  --size_threshold SIZE_THRESHOLD
                        Maximum file size (in GB). Default: 10.
  --rse RSE             Name of RSE to upload files to.
  --continue_uploads    Continue upload process after interrupted run.
  --max_duration MAX_DURATION
                        Maximum run time for the script in seconds. No new uploads will be initiated after this time. Default: 1 day.
  --scope SCOPE         Scope to upload files to. Default: user.$RUCIO_ACCOUNT.
  --lifetime LIFETIME   Lifetime of uploaded files in seconds. Do not set for infinite lifetime.
  --max_workers MAX_WORKERS
                        Run in parallel mode by setting the maximum number of available worker nodes. Default: 1.
  --debug               Enable DEBUG output instead of standard INFO.
```

## reconstruction.py

Files uploaded to the grid using the archiver.py script can be reconstructed into their original format by running the reconstruction.py script after downloading the relevant tarballs from the grid.
The information file contains a dictionary of which files are stored in which tarball and is automatically created during the uploading process with archiver.py.

```
usage: reconstruction.py [-h] --input_path INPUT_PATH [--information_file INFORMATION_FILE] [--target_dir TARGET_DIR] [--file_list FILE_LIST] [--debug]

optional arguments:
  -h, --help            show this help message and exit
  --input_path INPUT_PATH
                        Path to where hashed files are stored.
  --information_file INFORMATION_FILE
                        Name of JSON file containing information about the original files to reconstruct. Default: 'file_information.json'.
  --target_dir TARGET_DIR
                        Directory in which to place the reconstructed files. Default: './'.
  --file_list FILE_LIST
                        File containing list of files to reconstruct. File names are the keys in the information file, directory names can also be used. If no list is provided, all files are reconstructed.
  --debug               Enable DEBUG output instead of standard INFO.
```

## delete_rules.py

Given a list of files on a RSE, obtained with `rucio list-datasets-rse | grep xyz`, this script deletes the corresponding datasets.

```
usage: delete_rules.py [-h] -f FILENAME [-r RSE] [-d] [--noconfirm]

optional arguments:
  -h, --help            show this help message and exit
  -f FILENAME, --filename FILENAME
                        Name of file containing list of datasets to delete. Datasets are required to be listed as scope:datasetname with one dataset per row.
  -r RSE, --rse RSE     Name of RSE to delete rules from.
  -d, --debug           Enable DEBUG information instead of standard INFO level.
  --noconfirm           Do not ask for confirmation before deleting rule.
```

## replica_on_rse.py

Utility to check for a given list of datasets whether replicas are available on a given RSE. The list of datasets is provided as a text file with one dataset per line.

```
usage: replica_on_rse.py [-h] --rse RSE -i INPUTFILE

optional arguments:
  -h, --help            show this help message and exit
  --rse RSE             Name of RSE to check
  -i INPUTFILE, --inputfile INPUTFILE
                        Name of file containing one dataset name in the format scope:name per line
```
