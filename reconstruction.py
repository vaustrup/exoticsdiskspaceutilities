import argparse
import json
import logging
import os
import pathlib
import shutil
import sys

log = logging.getLogger("archiver")
tarball_name = ""

class Reconstructor:
    """
    Class Reconstructor

    Attributes
    ----------
    informationFile: str (default: 'file_information.json')
        name of file containing information about the hashed file names
    targetDir: str (default: './')
        path in which to place the recreated original files
    inputPath: str (default: './')
        path to where hashed files are stored
    fileList: list[str] (default: [])
        file containing list of files to reconstruct,
        omit to reconstruct everything
    """
    def __init__(self):
        """
        Creates instance of class Reconstructor
        """
        self.informationFile = "file_information.json"
        self.targetDir = "./"
        self.inputPath = "./"
        self.fileList = []

    def _read_file_information(self) -> dict[str, dict[str, str]]:
        """
        Read information about the files in the dataset.

        Returns:
            Dict with original filenames (including relative path) as keys
            and another Dict as value, containing hashed file names of chunked
            original file as keys and name of the tarball it is included in as value
        """
        log.info(f"Reading in information from {self.informationFile}.")
        with open(self.informationFile, "r") as f:
            file_information = json.load(f)
            return file_information
    
    def _matches_requested_files(self, file_name):
        """
        Check if name of given file matches the ones of files requested to be reconstructed

        Arguments:
            file_name: str
                name of file to match
        """
        if self.fileList == []:
            return True
        child = pathlib.PurePath(file_name)
        for name in self.fileList:
            parent = pathlib.PurePath(name)
            print(parent.parts, child.parts)
            if parent.parts == child.parts[:len(parent.parts)]:
                log.debug(f"File {file_name} matches list of files to be reconstructed.")
                return True 
        log.debug(f"File {file_name} does not match any filter.")
        return False

    def _unpack_tarball(self, tarball_name: str) -> None:
        log.debug(f"Need to unpack {tarball_name} before proceeding.")
        shutil.unpack_archive(f"{self.inputPath}/{tarball_name}", extract_dir=self.inputPath)        

    def _merge_file_chunks(self, merged_file_name: pathlib.Path, chunks: dict[str, str]) -> None:
        """
        Merge chunked files to recreate original file

        Arguments:
            merged_file_name: pathlib.Path
                path to the reconstructed file
            chunks: Dict[str, str]
                hashed file names as keys and names of tarballs they are included in as values
        """
        log.debug(f"Merging file chunks into {merged_file_name}.")
        with open(merged_file_name, "wb") as f_out:
            for hashed_file_name, tarball_name in chunks.items():
                file_chunk = pathlib.Path(f"{self.inputPath}/{hashed_file_name}")
                if not file_chunk.is_file():
                    self._unpack_tarball(tarball_name)
                with open(file_chunk, "rb") as f_in:
                    f_out.write(f_in.read())
    
    def reconstruct(self) -> None:
        """
        Reconstruct original files by reading relevant information from self.informationFile
        """
        log.info(f"Reconstructing original files into target directory {self.targetDir}.")
        file_dict = self._read_file_information()
        log.info(f"{len(file_dict.keys())} files can be reconstructed.")
        for orig_file, hashed_files in file_dict.items():
            if not self._matches_requested_files(orig_file):
                continue
            log.info(f"Reconstructing file {orig_file}.")
            file_path: pathlib.Path = self.targetDir / pathlib.Path(orig_file)
            file_directory: pathlib.Path = file_path.parent
            os.makedirs(file_directory, exist_ok=True)
            if len(hashed_files) == 1:
                # file was not split, move it to its place
                log.debug("File was not split, so simply move it into its place.")
                [(hashed_file_name, tarball_name)] = hashed_files.items()
                hashed_file = pathlib.Path(f"{self.inputPath}/{hashed_file_name}")
                if not hashed_file.is_file():
                    self._unpack_tarball(tarball_name)
                shutil.move(hashed_file, file_path)
            else:
                # we need to piece the chunks together
                self._merge_file_chunks(file_path, hashed_files)

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("--input_path",
                        dest="input_path",
                        required=True,
                        help="Path to where hashed files are stored.")
    parser.add_argument("--information_file",
                        dest="information_file",
                        default="file_information.json",
                        help="Name of JSON file containing information about the original files to reconstruct. Default: 'file_information.json'.")
    parser.add_argument("--target_dir",
                        dest="target_dir",
                        default="./",
                        help="Directory in which to place the reconstructed files. Default: './'.")
    parser.add_argument("--file_list",
                        dest="file_list",
                        default=None,
                        help="File containing list of files to reconstruct. File names are the keys in the information file, directory names can also be used. If no list is provided, all files are reconstructed.")
    parser.add_argument("--debug",
                        dest="debug",
                        action="store_true",
                        help="Enable DEBUG output instead of standard INFO.")

    args = parser.parse_args()

    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter("%(asctime)s [%(levelname)4s]: %(message)s", "%d.%m.%Y %H:%M:%S")
    handler.setFormatter(formatter)
    log.addHandler(handler)
    log.setLevel(logging.INFO)
    if args.debug:
        log.setLevel(logging.DEBUG) 

    reconstructor = Reconstructor()
    reconstructor.inputPath = args.input_path
    reconstructor.informationFile = args.information_file
    reconstructor.targetDir = args.target_dir
    file_list = []
    if not args.file_list is None:
        with open(args.file_list, "r") as f:
            for file_name in f:
                file_list.append(file_name.replace("\n", ""))
    reconstructor.fileList = file_list
    reconstructor.reconstruct()

if __name__ == "__main__":
    main()
