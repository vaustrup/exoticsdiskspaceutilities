"""
Utility to check for a given list of datasets whether replicas are available on a given RSE.
The list of datasets is provided as a text file with one dataset per line.
"""

import argparse

from rucio.client import Client


def dataset_on_rse(dataset, rse):
    """
    Arguments:
        dataset (str): name of dataset in the format scope:name
        rse (str): name of RSE

    Returns True if a replica for the given dataset is available on the given RSE, False otherwise
    """
    scope, name = dataset.split(":")
    replicas = client.list_dataset_replicas(scope=scope, name=name)
    for replica in replicas:
        if replica["rse"] == rse:
            return True
    return False

parser = argparse.ArgumentParser(description=__doc__)

parser.add_argument("--rse",
                   dest="rse",
                   required=True,
                   help="Name of RSE to check")
parser.add_argument("-i", "--inputfile",
                   dest="inputfile",
                   required=True,
                   help="Name of file containing one dataset name in the format scope:name per line")

args=parser.parse_args()


client = Client()

with open(args.inputfile) as f:
    for line in f:
        dataset = line.strip()
        if not dataset_on_rse(dataset, args.rse):
            print(f"Cannot find replica for {dataset} on {args.rse}.")
        
