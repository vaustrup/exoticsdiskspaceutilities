'''
Check whether the README.md is up-to-date by comparing to the output of --help
Raises an exception if --help output is not found in README.md
'''

import argparse
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--script", required=True, help="Script to check README against.")
args = parser.parse_args()

# first, run the given script with the --help flag and capture the output
command = ["python3", args.script, "--help"]
try:
    help_output = subprocess.check_output(command, stderr=subprocess.STDOUT, universal_newlines=True)
except subprocess.CalledProcessError as e:
    help_output = e.output

# then, compare to README file
with open("README.md", "r") as readme_file:
    readme_content = readme_file.read()

    # whitespaces and linebreaks are not the same for help_output and for readme_content
    # but we do not really care about those, so just compare the simplified strings instead
    cleaned_help = ''.join(help_output.split())
    cleaned_readme = ''.join(readme_content.split())
    # some versions use "options:" for optional arguments, other use "optional arguments:"
    cleaned_help = cleaned_help.replace("options:", "optionalarguments:")
    cleaned_readme = cleaned_readme.replace("options:", "optionalarguments:")
    if cleaned_help not in cleaned_readme:
        print(cleaned_help)
        print(cleaned_readme)
        raise Exception("Help text not found in README.md. Please check the README is up-to-date.")

