import argparse
import csv
import datetime
import logging
import math
import os
import sys

log = logging.getLogger(__name__)

handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter("%(asctime)s [%(levelname)4s]: %(message)s", "%d.%m.%Y %H:%M:%S")
handler.setFormatter(formatter)
log.addHandler(handler)
log.setLevel(logging.INFO)

from analyses import GLANCE_IDS

ROOT ='/eos/atlas/atlascerngroupdisk/phys-exotics/'

GLANCE_LINK = "https://atlas-glance.cern.ch/atlas/analysis/analyses/details.php?id="

parser = argparse.ArgumentParser()

parser.add_argument("--exclude",
        dest="exclude",
        default=[],
        help="Comma-separated list of names of subdirectories to exclude")

args = parser.parse_args()


def _create_csv(subgroup):
    with open(subgroup+'.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(["Name", "Glance", "Number of Files", "Disk Space", "Last modified"])

def _write_to_csv(subgroup, analysis, glance_ids, file_counter, size, date):
    glance_entries = ""
    if isinstance(glance_ids, list):
        for glance_id in glance_ids:
            glance_entries += GLANCE_LINK + str(glance_id) + " "
    elif glance_ids is not None:
        if glance_ids == "???":
            glance_entries = "???"
        glance_entries = GLANCE_LINK + str(glance_ids)
    with open(subgroup+'.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow([analysis,glance_entries,file_counter,size,date])

def convert_file_size(size):
    if size == 0:
        return "0B"
    i = int(math.floor(math.log(size, 1024)))
    p = math.pow(1024, i)
    s = round(size / p, 2)
    units = ["B", "kB", "MB", "GB", "TB"]
    return f"{s} {units[i]}"

def newest_file(subgroup, analysis):
    date = 0
    last_accessed_date = 0
    total_size = 0
    latest_file = ""
    last_accessed_file = ""
    file_counter = 0
    log.info(f"Checking analysis {analysis}")
    for path, subdirs, files in os.walk(os.path.join(ROOT, subgroup, analysis)):
        for name in files:
            file_counter+=1
            f = os.path.join(path, name)
            if os.path.islink(f):
                continue
            total_size += os.path.getsize(f)
            if os.path.getmtime(f) > datetime.datetime.now().timestamp():
                log.warning(f"Something weird is going on, file {f} seems to come from the future? Timestamp is {os.path.getmtime(f)}. Skipping")
                continue
            if os.path.getatime(f) > last_accessed_date:
                last_accessed_date = os.path.getatime(f)
                last_accessed_file = f
            if os.path.getmtime(f) > date:
                latest_file = f
                date = os.path.getmtime(f)
    if latest_file != "":
        log.info(f"Last accessed: {datetime.datetime.fromtimestamp(last_accessed_date).strftime('%Y-%m-%d')} for file {last_accessed_file}")
        log.info(f"Total size: {convert_file_size(total_size)}")
        log.info(f"Number of files: {file_counter}")
        try:
            log.info(f"Latest modified: file {latest_file} at {datetime.datetime.fromtimestamp(date)}")
        except ValueError:
            log.info(f"Cannot get date, use timestamp instead. Latest modified: {latest_file} at {date}")

    glance_id = None
    if subgroup in GLANCE_IDS and analysis in GLANCE_IDS[subgroup]:
        glance_id = GLANCE_IDS[subgroup][analysis]
    _write_to_csv(subgroup, analysis, glance_id, file_counter, convert_file_size(total_size), datetime.datetime.fromtimestamp(date).strftime('%Y-%m-%d') if latest_file!="" else None)

subgroups = [name for name in os.listdir(ROOT) if os.path.isdir(os.path.join(ROOT, name)) and name not in args.exclude]
for subgroup in subgroups:
    analyses = [name for name in os.listdir(os.path.join(ROOT, subgroup)) if os.path.isdir(os.path.join(ROOT, subgroup, name))]
    _create_csv(subgroup)
    for analysis in analyses:
        newest_file(subgroup, analysis)

