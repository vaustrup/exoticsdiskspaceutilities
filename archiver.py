from rucio.client import Client
from rucio.client.uploadclient import UploadClient
from rucio.common.exception import DataIdentifierNotFound, DataIdentifierAlreadyExists, FileAlreadyExists, NoFilesUploaded, NotAllFilesUploaded, RSEWriteBlocked

import argparse
import concurrent.futures
from datetime import date
import hashlib
import json
import logging
import os
import pathlib
import requests
import shutil
import signal
import subprocess
import sys
import tarfile
import time

log = logging.getLogger("archiver")


class Archiver:
    """
    Class Archiver

    Attributes
    ----------
    scope: str (default: "")
        Scope to upload files and register dataset to
    lifetime: int|None (default: None)
        Lifetime of uploaded files in seconds
    sizeThreshold: float (default: 10)
        Maximum size of files to upload in GB
    tmpDir: str (default: "")
        Name of temporary directory to store temporary files in
    rse: str (default: "")
        Name of RSE to upload files and register dataset to
    inputFileNames: list[str] (default: [])

    inputPath: str (default: "")

    continueUploads: bool (default: False)
        Whether to continue the uploading process or to start fresh
    maxDuration: int (default: 86400)
        Maximum duration for one upload in seconds
    maxWorkers: int (default: 1)
        Number of workers to use for uploading files in parallel
    """
    def __init__(self):
        """
        Constructs an object of class Archiver.
        """
        self.scope: str = ""
        self.sizeThreshold: float = 10. # in GB
        self._client: Client = Client()
        self._uploadClient = UploadClient(self._client, logger=log)
        self._datasetName: str = ""
        self.tmpDir: pathlib.Path = ""
        self.rse: str = ""
        self._successfulUploads: list[str] = []
        self._unsuccessfulUploads: list[str] = []
        self.inputFileNames: list[str] = []
        self.inputPath: str = ""
        self.continueUploads: bool = False
        self._startTime: float = time.time()
        self.maxDuration: int = 24 * 60 * 60
        self.lifetime: int|None = None
        self.maxWorkers: int = 1
        self._interruptCounter: int = 0
        self._tarballDict: dict[str, dict[str, str]] = {}
        self._hashDict: dict[str, str] = {}
        # register interrupt signal handler
        signal.signal(signal.SIGINT, self._handle_interrupt)

    @property
    def datasetName(self):
        return self._datasetName
    
    @datasetName.setter
    def datasetName(self, dataset_name: str):
        if len(dataset_name) > 100:
            raise Exception("Rucio has a limitation on length of filenames. Please choose a name with less than 100 characters.")
        self._datasetName = dataset_name
    
    @staticmethod
    def _get_file_size(file_name: str) -> float:
        """
        Get size of file on disk in GB.
        Arguments:
            file_name: str
                Name of file to get size of
        Returns:
            size of file in GB
        """
        # Divide by 1024^3 to go from bytes to GB
        return os.path.getsize(file_name) / (1024. * 1024. * 1024.)
    
    def create_dataset(self) -> None:
        """
        Create dataset in rucio if it does not already exist.
        """
        log.info(f"Checking if dataset {self.datasetName} in scope {self.scope} needs to be created.")
        try:
            self._client.add_dataset(scope=self.scope, name=self.datasetName, rse=self.rse)
        except DataIdentifierAlreadyExists:
            log.info(f"No need to create dataset {self.datasetName} in scope {self.scope} as it already exists.")
            return
        log.info(f"Creating dataset {self.datasetName} in scope {self.scope}.")

    def create_readme(self) -> None:
        """
        Create README file with instructions on how to reconstruct the input files from the uploaded tarballs.
        """
        reconstruction_code = requests.get("https://gitlab.cern.ch/vaustrup/exoticsdiskspaceutilities/-/raw/main/reconstruction.py").text
        readme_text = f"""
        This dataset is the tape archive originally taken from {self.inputPath}
        The copy was performed on {date.today()}.
        The data is archived using the archiver.py script from https://gitlab.cern.ch/vaustrup/exoticsdiskspaceutilities/ using the following command:
        python3 archiver.py --size_threshold {self.sizeThreshold} --tmp_dir {self.tmpDir} --input_path {self.inputPath} --dataset_name {self.datasetName} --rse {self.rse}
        Files are renamed to a hash to maintain the simple structure required by rucio and then combined into tarballs.
        Large files are split into chunks to facilitate the uploading process.

        Information about the splitted files and the contents of the tarballs can be found in the file information_file.json, included in the dataset, in the following format:
            'original_filename_1':
                'hashed_chunk_1': 'tarball_file_X'
                'hashed_chunk_2': 'tarball_file_Y'
            'original_filename_2':
                'hashed_chunk_3': 'tarball_file_Z'
                'hashed_chunk_4': 'tarball_file_Y'
                'hashed_chunk_5': 'tarball_file_X'

        To restore the original dataset, download and unpack all tarballs of the dataset.
        Then run the reconstruction.py script from https://gitlab.cern.ch/vaustrup/exoticsdiskspaceutilities/ like so:
        python3 reconstruction.py --target_dir /path/to/output/directory/ --input_path /path/to/directory/containing/unpacked/tarballs --information_file /path/to/information_file.json        
        The python script to reconstruct the files is included below for future reference:

    	{reconstruction_code}
        """

        with open(self.tmpDir / (self.datasetName + "README"), "w") as f:
            f.write(readme_text)

    def _split_file(self, file_name: str) -> list[str]:
        '''
        Split files larger than the maximum file size threshold into smaller chunks below a given size threshold.
        Arguments:
            file_name: str
                Name of file to split into chunks
        Returns:
            List of file chunks
        '''
        log.info(f"File {file_name} is larger than {self.sizeThreshold} GB. Storing chunks in {self.tmpDir}.")
        os.makedirs(self.tmpDir, exist_ok=True)
        splitted_files: list[str] = []
        relative_file_path = os.path.relpath(file_name, self.inputPath)
        self._tarballDict[relative_file_path] = {}
        # Calculate the number of chunks
        file_size = self._get_file_size(file_name)
        number_of_chunks = int(file_size // self.sizeThreshold)
        if file_size % self.sizeThreshold != 0:
            number_of_chunks += 1
        log.debug(f"File has a size of {file_size} GB. Creating {number_of_chunks} chunks of data.")
        with open(file_name, 'rb') as f:
            for chunk_index in range(number_of_chunks):
                # Read a chunk of data
                chunk_data = f.read(int(self.sizeThreshold * 1024 * 1024 * 1024))
            
                # Create a chunk file
                hashed_chunk_name = hashlib.md5(f'{file_name}_chunk{chunk_index}'.encode()).hexdigest()
                self._hashDict[hashed_chunk_name] = relative_file_path
                chunk_file_path = self.tmpDir / hashed_chunk_name
                with open(chunk_file_path, 'wb') as chunk_file:
                    chunk_file.write(chunk_data)
                    splitted_files.append(chunk_file_path)
        return splitted_files
    
    def _handle_normal_file(self, file_name: str) -> pathlib.Path:
        """
        Copy file with size below the threshold to tmpDir and store them as their hashed name.
        Arguments:
            file_name: str
                Name of file to copy and rename
        Returns:
            Name of hashed file in tmpDir
        """
        hashed_file_name = hashlib.md5(file_name.encode()).hexdigest()
        relative_file_path = os.path.relpath(file_name, self.inputPath)
        self._tarballDict[relative_file_path] = {}
        self._hashDict[hashed_file_name] = relative_file_path
        shutil.copy2(file_name, self.tmpDir / hashed_file_name)
        return self.tmpDir / hashed_file_name

    def _collect_files(self, files: list[str]) -> list[list[str]]:
        '''
        Collect files such that their sum is smaller than the maximum file size threshold.
        Create sets of roughly equal sizes.
        Arguments:
            files: list[str]
                Files to collect into sets of files with total sizes below certain threshold
        Returns:
            List of files sorted into sets: list[list[str]]
        '''
        log.info(f"Collecting {len(files)} file{'s' if len(files)>1 else ''} into sets not exceeding {self.sizeThreshold} GB. This may take a while.")
        # first, get all files, split them if necessary, and store their sizes
        file_names_and_sizes: dict[str, float] = {}
        for file in files:
            if self._get_file_size(file) > self.sizeThreshold:
                for f in self._split_file(file):
                    file_names_and_sizes[f] = self._get_file_size(f)
            else:
                file_name = self._handle_normal_file(file)
                file_names_and_sizes[file_name] = self._get_file_size(file_name)
        # then sort them by their size
        sorted_files: list[str] = sorted(file_names_and_sizes, key=lambda x: file_names_and_sizes[x], reverse=True)
        
        # finally, create sets of files not exceeding the given threshold
        total_size: float = 0
        current_set: list[str] = []
        file_sets: list[list[str]] = []
        for file in sorted_files:
            file_size = file_names_and_sizes[file]
            if total_size + file_size <= self.sizeThreshold:
                current_set.append(file)
                total_size += file_size
            else:
                file_sets.append(current_set)
                current_set = [file]
                total_size = file_size
        # add the remaining files
        if current_set:
            file_sets.append(current_set)
        log.info(f"Files have been collected in {len(file_sets)} {'sets' if len(file_sets)>1 else 'set'} of files.")
        return file_sets

    def _create_tarballs(self, file_sets: list[list[str]]) -> list[str]:
        '''
        Build tarballs of roughly equal size to reduce number of files to upload
        Arguments:
            file_sets: list[list[str]]
                Sets of files to put into tarballs
        Returns:
            List of tarballs in tmpDir: list[str]
        '''
        tarball_names = []
        log.info(f"Building tarballs in {self.tmpDir}.")
        for file_set_index, file_set in enumerate(file_sets):
            tarball_name = self.datasetName + str(file_set_index) + ".tar.gz"
            self._create_tarball_from_fileset(file_set, tarball_name)
            tarball_names.append(tarball_name)
            for file in file_set:
                hashed_file_basename = os.path.basename(file)
                orig_file_name = self._hashDict[hashed_file_basename]
                self._tarballDict[orig_file_name][hashed_file_basename] = tarball_name
        tarball_list_name = self.tmpDir / (self.datasetName + "tarball_list.json")
        log.info(f"Writing list of files to {tarball_list_name} for future reconstruction.")
        with open(tarball_list_name, "w") as json_file:
            json.dump(self._tarballDict, json_file)
        log.info(f"Successfully created {len(tarball_names)} tarballs.")
        return tarball_names

    def _create_tarball_from_fileset(self, files: list[str], tarball_name: str) -> None:
        '''
        Build tarball from given set of files, remove temporary input files.
        Arguments:
            files: list[str]
                List of files to put into a tarball
            tarball_name: str
                Name of the tarball file to create
        '''
        log.debug(f"Creating tarball {tarball_name} containing {len(files)} file{'s' if len(files)>1 else ''}.")
        with tarfile.open(self.tmpDir / tarball_name, "w:gz") as tar:
            for f in files:
                # need to figure out the path relative to the temporary directory
                # because we do not want to store the complete folder structure
                relative_name = os.path.relpath(f, self.tmpDir)
                tar.add(f, arcname=relative_name)
                
        for f in files:
            os.remove(f)
        log.debug(f"Successfully created tarball {tarball_name} and deleted temporary files.")

    def prepare_inputs(self) -> None:
        '''
        Recursively create list of input file names to upload.
        '''

        # clean up leftovers from previous runs
        if os.path.exists("successful_uploads.txt"):
            os.remove("successful_uploads.txt")
        if os.path.exists("unsuccessful_uploads.txt"):
            os.remove("unsuccessful_uploads.txt")

        # otherwise create tmp directory if it does not exist yet
        os.makedirs(self.tmpDir, exist_ok=True)
        tarball_list = self.tmpDir / (self.datasetName + "tarball_list.json")
            
        if self.continueUploads:
            # if we want to continue from previous run, just use the tarballs already created
            log.info(f"Reading in existing tarballs in tmp directory {self.tmpDir}")
            tarball_names = self.tmpDir.glob("*.tar.gz")
            files = tarball_names
            # files.append(self.tmpDir / tarball_list)
            if os.path.exists("successful_uploads.txt"):
                log.info("Reading in previously uploaded files.")
                with open("successful_uploads.txt", "r") as f:
                    for line in f:
                        self._successfulUploads.append(line)
        # otherwise, collect files into sets and create tarballs
        else:
            log.debug("Cleaning up potential temporary files from previous run.")
            for file in self.tmpDir.glob("*"):
                os.remove(file)
            input_files = []
            for r, _, f in os.walk(self.inputPath):
                for file in f:
                    input_files.append(os.path.join(r,file))
            file_sets = self._collect_files(input_files)
            files = self._create_tarballs(file_sets)

        if tarball_list not in self._successfulUploads:
            self.inputFileNames.append(tarball_list)
        self.create_readme()
        if self.tmpDir / (self.datasetName + "README") not in self._successfulUploads:
            self.inputFileNames.append(self.tmpDir / (self.datasetName + "README") )
        self.inputFileNames.extend([self.tmpDir / f for f in files if f not in self._successfulUploads])
        

    def upload_files(self) -> None:
        '''
        Upload files with rucio to given RSE.
        '''
        log.info(f"Uploading files to RSE {self.rse}.")
        with concurrent.futures.ThreadPoolExecutor(max_workers=self.maxWorkers) as executor:
            files = [file for file in self.inputFileNames if file not in self._successfulUploads]
            upload_tasks = [executor.submit(self._upload_file, file) for file in files]
            start_time = time.time()
            concurrent.futures.wait(upload_tasks, timeout=self.maxDuration-(time.time() - start_time))

    def _upload_file(self, file_name: str, retries=0, max_retries=3) -> None:
        '''
        Upload given file to RSE and attach it to dataset
        Arguments:
            file_name: str
                Name of file to upload
            retries: int (default: 0)
                Number of previous retries to upload the file
            max_retries: int (default: 3)
                Maximum allowed number of retries to upload the file
        '''
        if retries >= max_retries:
            log.error(f"Failed to upload file {file_name}.")
            return
        
        file_dict = {
            'path': file_name,
            'rse': self.rse,
            'did_scope': self.scope,
            'did_name': os.path.relpath(file_name, self.tmpDir),
            'dataset_scope': self.scope,
            'dataset_name': self.datasetName,
            'lifetime': self.lifetime,
        }

        log.info(f"Uploading file {file_name}.")
        try:
            #self._uploadClient.upload([file_dict])
            command = f"rucio upload {file_dict['path']} --scope {file_dict['did_scope']} --name {file_dict['did_name']} --lifetime {file_dict['lifetime']} --rse {file_dict['rse']}"
            subprocess.run(command.split(" "))
        except (NoFilesUploaded, NotAllFilesUploaded) as e:
            if retries < max_retries:
                log.warning(f"{file_name} could not be uploaded due to the following error: ")
                log.warning(f"{e}")
                log.warning("Retrying.")
                self._upload_file(file_name, retries=retries+1)
            log.error(f"{file_name} could not be uploaded due to the following error: ")
            log.error(f"{e}")
            log.error("Skipping this file.")
        except RSEWriteBlocked as e:
            log.error(f"Could not write to RSE {self.rse}")
            log.error(f"{e}")
        log.debug(f"Uploaded file {file_name}.")

    def _file_upload_successful(self, file_name: str) -> bool:
        '''
        Check if file is available on rucio
        Arguments:
            file_name: str
                Name of file to check upload for
        Returns:
            File upload successful: bool
        '''
        file = os.path.basename(file_name)
        log.debug(f"Checking if file {file} has been successfully uploaded.")
        try:
            did_info = self._client.get_metadata(self.scope, file)
        except DataIdentifierNotFound:
            log.error(f"Could not find DID with scope {self.scope} and name {file}.")
            self._unsuccessfulUploads.append(file)
            return False
        upload_successful = did_info["availability"] == 'AVAILABLE'
        if upload_successful:
            log.debug(f"File {file} has been successfully uploaded.")
            self._successfulUploads.append(file)
            return True
        log.debug(f"File {file} has not been successfully uploaded.")
        self._unsuccessfulUploads.append(file)
        return False
    
    def _handle_interrupt(self, signum, frame):
        log.info("Detected CTRL+C. Exiting gracefully.")
        self._interruptCounter += 1
        if self._interruptCounter > 1:
            log.info("Interrupt called more than once. Exiting immediately...")
            sys.exit()
        self.finalize()
    
    def finalize(self) -> None:
        '''
        Check if all files have been successfully uploaded. If at least one of them has not been, log an error and exit.
        If all files have been successfully uploaded close the dataset.
        '''
        log.info("Finalizing upload procedure.")
        log.info("Checking if all files have been successfully uploaded.")
        for input_file_name in self.inputFileNames:
            if not self._file_upload_successful(input_file_name):
                log.error(f"File with name {input_file_name} has not been uploaded correctly.")
        # TODO need to make sure files are not added here repeatedly
        with open("successful_uploads.txt", "w") as f:
            for file in self._successfulUploads:
                f.write(file+"\n")
        with open("unsuccessful_uploads.txt", "w") as f:
            for file in self._unsuccessfulUploads:
                f.write(file+"\n")
        if self._unsuccessfulUploads:
            log.error("Not all files have been uploaded correctly. Please check carefully. Will not close the dataset.")
            return
        for input_file_name in self.inputFileNames:
            base_name = os.path.basename(input_file_name)
            file_dict = {
                "scope": self.scope,
                "name": base_name,
            }
            log.debug(f"Attaching {base_name} to dataset {self.datasetName} in scope {self.scope}.")
            try:
                self._client.attach_dids(scope=self.scope, name=self.datasetName, rse=self.rse, dids=[file_dict])
            except FileAlreadyExists:
                log.info(f"Cannot attach file {base_name} to dataset {self.datasetName} in scope {self.scope} as it already exists.")
        try:
            log.info(f"Closing dataset {self.datasetName} in scope {self.scope}")
            self._client.close(scope=self.scope, name=self.datasetName)
        except DataIdentifierNotFound:
            log.error(f"Could not close dataset because DID with scope {self.scope} and name {self.datasetName} was not found.")
            return
        log.info("Cleaning up temporary files.")
        os.remove("successful_uploads.txt")
        os.remove("unsuccessful_uploads.txt")
        #for file in self.tmpDir.glob("*"):
        #    os.remove(file)



def main():

    parser = argparse.ArgumentParser(description=main.__doc__)

    parser.add_argument("-d", "--dry_run",
                        dest="dry_run",
                        action="store_true",
                        help="Collect input files and create tarballs, but do not actually upload them with Rucio.")
    parser.add_argument("--dataset_name",
                        dest="dataset_name",
                        required=True,
                        help="Name of dataset to register files in.")
    parser.add_argument("--tmp_dir",
                        dest="tmp_dir",
                        default=f"/tmp/{os.environ.get('USER')}/archiver",
                        help=f"Directory to temporarily store file chunks and tarballs to upload. Default: /tmp/$USER/archiver.")
    parser.add_argument("--input_path",
                        dest="input_path",
                        required=True,
                        help="Path to the directory containing all files to be uploaded.")
    parser.add_argument("--size_threshold",
                        dest="size_threshold",
                        default=10,
                        help="Maximum file size (in GB). Default: 10.")
    parser.add_argument("--rse",
                        dest="rse",
                        required=True,
                        help="Name of RSE to upload files to.")
    parser.add_argument("--continue_uploads",
                        dest="continue_uploads",
                        action="store_true",
                        help="Continue upload process after interrupted run.")
    parser.add_argument("--max_duration",
                        dest="max_duration",
                        default=24*60*60,
                        help="Maximum run time for the script in seconds. No new uploads will be initiated after this time. Default: 1 day.")
    parser.add_argument("--scope",
                        dest="scope",
                        default=f"user.{os.environ.get('RUCIO_ACCOUNT')}",
                        help="Scope to upload files to. Default: user.$RUCIO_ACCOUNT.")
    parser.add_argument("--lifetime",
                        dest="lifetime",
                        default=None,
                        help="Lifetime of uploaded files in seconds. Do not set for infinite lifetime.")
    parser.add_argument("--max_workers",
                        dest="max_workers",
                        default=1,
                        help="Run in parallel mode by setting the maximum number of available worker nodes. Default: 1.")
    parser.add_argument("--debug",
                        dest="debug",
                        action="store_true",
                        help="Enable DEBUG output instead of standard INFO.")

    args = parser.parse_args()

    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter("%(asctime)s [%(levelname)4s]: %(message)s", "%d.%m.%Y %H:%M:%S")
    handler.setFormatter(formatter)
    log.addHandler(handler)
    log.setLevel(logging.INFO)
    if args.debug:
        log.setLevel(logging.DEBUG)

    archiver = Archiver()
    archiver.datasetName = args.dataset_name
    archiver.sizeThreshold = float(args.size_threshold)
    archiver.tmpDir = pathlib.Path(args.tmp_dir)
    archiver.continueUploads = args.continue_uploads
    archiver.maxDuration = args.max_duration
    archiver.rse = args.rse
    archiver.scope = args.scope
    archiver.lifetime = args.lifetime
    archiver.maxWorkers = int(args.max_workers)
    archiver.inputPath = args.input_path

    log.info("Initiating upload process.")
    log.info(f"Using {archiver.maxWorkers} worker{'s' if archiver.maxWorkers>1 else ''} to upload.")
    log.info(f"Uploading to dataset {archiver.datasetName}.")
    log.info(f"Uploading to RSE {archiver.rse}.")
    log.info(f"Uploading to scope {archiver.scope}.")
    if archiver.lifetime == None:
        log.info("Uploaded files will have infinite lifetime.")
    else:
        log.info(f"Lifetime of uploaded files set to {archiver.lifetime} seconds.")
    if archiver.continueUploads:
        log.info("Continuing from previous upload process.")
    if args.dry_run:
        log.info("This is a dry run. Files will be split into chunks and tarballs will be created, but no files will be uploaded.")

    archiver.prepare_inputs()
    if not args.dry_run:
        archiver.create_dataset()
        archiver.upload_files()
        archiver.finalize()

if __name__ == "__main__":
    main()
